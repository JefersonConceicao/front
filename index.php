<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt-br">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Lidera Clothes </title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="">

    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/jqvmap/dist/jqvmap.min.css">


    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


</head>
  

  <style>
.mySlides {display:none}
</style>

<body>
   







    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>

                <a class="navbar-brand" href="index.php"> Lidera Clothes </a>
              
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="index.php"> <i class="menu-icon fa fa-home"></i> Inicio </a>
                    </li>
                    <h3 class="menu-title"> Gerenciamento </h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users">

                        </i>Gerenciar Funcionarios</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus-square-o"></i><a href="cadastrofuncionario.php">Cadastrar Funcionários</a></li>
                            <li><i class="fa fa-search-plus"></i><a href="visualizarfuncionario.php">Visualizar Funcionários</a></li>
                           
                        </ul>

                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-shopping-cart">

                        </i>Gerenciar Pedidos</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus-square-o"></i><a href="cadastrarpedido.php">Cadastrar Pedidos</a></li>
                            <li><i class="fa fa-search-plus"></i><a href="visualizarpedido.php">Visualizar Pedidos</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user">

                        </i>Gerenciar Clientes</a>
            <ul class="sub-menu children dropdown-menu">
           <li><i class="menu-icon fa fa-user"></i><a href="cadastrarmateriais.php">Cadastrar Clientes</a></li>


     <li><i class="menu-icon fa fa-search-plus"></i> <a href="visualizarmateriais.php">
      Visualizar Clientes</a></li>

           
                        </ul>
                    </li> 

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-truck">

                        </i>Gerenciar Parceiros</a>
            <ul class="sub-menu children dropdown-menu">
           <li><i class="menu-icon fa fa-plus-square-o"></i><a href="cadastrarfornecedor.php">Cadastrar Empresa Parceira</a></li>


     <li><i class="menu-icon fa fa-search-plus"></i> <a href="visualizarfornecedor.php">
      Visualizar Empresas Parceiras</a></li>

                    </ul>
                </li>  
                    

                  
                    <h3 class="menu-title">Extras</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Páginas</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="pagelogin.php">Sair</a></li>
                    
                            <li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.php">Esqueceu a senha</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    <div class="header-left">
                     


        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                       
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                           
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        

           <div class="w3-container">
            <br> 
  <h2> <center>  </center> </h2> 
  


<div class="w3-content" style="max-width:800px">
  <img class="mySlides" src="images/lul.jpg" style="width:100%">
  <img class="mySlides" src="images/lul4.png" style="width:100%">
  <img class="mySlides" src="images/front7.jpg" style="width:100%">
</div>


   <br> 
   

<div class="w3-center">

  <button class="w3-button demo" onclick="currentDiv(1)">1</button> 
  <button class="w3-button demo" onclick="currentDiv(2)">2</button> 
  <button class="w3-button demo" onclick="currentDiv(3)">3</button> 
</div>

<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-blue", "");
  }
  x[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " w3-blue";
}

</script>
<br>
      
  





















        <div class="col-sm-12 mb-4">
        <div class="card-group">
            <div class="card col-md-6 no-padding ">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-users"></i>
                    </div>

                    <div class="h4 mb-0">

                      
                      
   <?php

include 'backend/Model/bancodedados.php';

 $banco = new bancodedados();


 $select = "SELECT * FROM funcionario";

 $query = mysqli_query($banco->conectabanco(),$select);

 $linhas = mysqli_num_rows($query);

                       
                   

                    echo "<span class='class=count'> $linhas  </span>";
                    
                    
                    ?>



                    
                  

                     </div>

                
                   <a href="visualizarfuncionario.php"> <small class="text-muted text-uppercase font-weight-bold">Funcionários</small> </a>
                    <div class="progress progress-xs mt-3 mb-0 bg-flat-color-1" style="width: 40%; height: 5px;"></div>
                </div>
            </div>           

               
            <div class="card col-md-6 no-padding ">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="h4 mb-0">
  


                    <?php


 $banco = new bancodedados();

 $select = "SELECT * FROM pedido";

 $query = mysqli_query($banco->conectabanco(),$select);

 $linhas = mysqli_num_rows($query);

                       

                    echo  " <span class='count'> $linhas </span>";

                          


                          ?>
                    </div>
                 <a href="visualizarpedido.php">   <small class="text-muted text-uppercase font-weight-bold">Pedidos</small> </a>
                    <div class="progress progress-xs mt-3 mb-0 bg-flat-color-2" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-md-6 no-padding ">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="h4 mb-0">
                <?php


                    $banco = new bancodedados();


                    $select = "SELECT * FROM cliente";

                    $query = mysqli_query($banco->conectabanco(),$select);
    
                    $linhas = mysqli_num_rows($query);



                     echo " <span class='count'>$linhas</span>";

                     ?>
                    </div>
                 <a href="visualizarmateriais.php">  <small class="text-muted text-uppercase font-weight-bold">Clientes</small></a>
                    <div class="progress progress-xs mt-3 mb-0 bg-flat-color-3" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-md-6 no-padding ">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-truck"></i>
                    </div>
                    <div class="h4 mb-0">
                    <?php


$banco = new bancodedados();


$select = "SELECT * FROM fornecedor";

$query = mysqli_query($banco->conectabanco(),$select);

$linhas = mysqli_num_rows($query);



                        



                     echo " <span class='count'>$linhas</span>";

                     ?>
                    </div>
                   <a href="visualizarfornecedor.php"><small class="text-muted text-uppercase font-weight-bold">Parceiros</small></a> 
                    <div class="progress progress-xs mt-3 mb-0 bg-flat-color-4" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
          
        
    </div>


















              
            <!--/.col-->

         
    <!-- Right Panel -->

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


    <script src="vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="assets/js/dashboard.js"></script>
    <script src="assets/js/widgets.js"></script>
    <script src="vendors/jqvmap/dist/jquery.vmap.min.js"></script>
    <script src="vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script>
        (function($) {
            "use strict";

            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#1de9b6', '#03a9f5'],
                normalizeFunction: 'polynomial'
            });
        })(jQuery);
    </script>

</body>

</html>
