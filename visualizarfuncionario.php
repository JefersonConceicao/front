<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt-br">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>  </title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="">

    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/jqvmap/dist/jqvmap.min.css">


    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body>


    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>

                <a class="navbar-brand" href="index.php"> Lidera Clothes  </a>
              
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="index.php"> <i class="menu-icon fa fa-home"></i> Inicio </a>
                    </li>
                    <h3 class="menu-title"> Gerenciamento </h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users">

                        </i>Gerenciar Funcionarios</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus-square-o"></i><a href="cadastrofuncionario.php">Cadastrar Funcionários</a></li>
                            <li><i class="fa fa-search-plus"></i><a href="visualizarfuncionario.php">Visualizar Funcionários</a></li>
                           
                        </ul>

                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-shopping-cart">

                        </i>Gerenciar Pedidos</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus-square-o"></i><a href="cadastrarpedido.php">Cadastrar Pedidos</a></li>
                            <li><i class="fa fa-search-plus"></i><a href="visualizarpedido.php">Visualizar Pedidos</a></li>
                        </ul>
                    </li>

                  <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-dropbox">

                        </i>Gerenciar Clientes</a>
            <ul class="sub-menu children dropdown-menu">
           <li><i class="menu-icon fa fa-plus-square-o"></i><a href="cadastrarmateriais.php">Cadastrar Clientes</a></li>


     <li><i class="menu-icon fa fa-search-plus"></i> <a href="visualizarmateriais.php">
      Visualizar Clientes</a></li>
                        </ul>
                    </li>


                             
                             <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-truck">

                        </i>Gerenciar Parceiros</a>
            <ul class="sub-menu children dropdown-menu">
           <li><i class="menu-icon fa fa-plus-square-o"></i><a href="cadastrarfornecedor.php">Cadastrar Fornecedor</a></li>


     <li><i class="menu-icon fa fa-search-plus"></i> <a href="visualizarfornecedor.php">
      Visualizar Parceiros</a></li>

                    </ul>
                </li>  


                    <h3 class="menu-title">Icons</h3><!-- /.menu-title -->

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Icons</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-fort-awesome"></i><a href="font-fontawesome.html">Font Awesome</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="font-themify.html">Themefy Icons</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="widgets.html"> <i class="menu-icon ti-email"></i>Widgets </a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Charts</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-line-chart"></i><a href="charts-chartjs.html">Chart JS</a></li>
                            <li><i class="menu-icon fa fa-area-chart"></i><a href="charts-flot.html">Flot Chart</a></li>
                            <li><i class="menu-icon fa fa-pie-chart"></i><a href="charts-peity.html">Peity Chart</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Maps</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-map-o"></i><a href="maps-gmap.html">Google Maps</a></li>
                            <li><i class="menu-icon fa fa-street-view"></i><a href="maps-vector.html">Vector Maps</a></li>
                        </ul>
                    </li>
                    <h3 class="menu-title">Extras</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-login.html">Login</a></li>
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-register.html">Register</a></li>
                            <li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.html">Forget Pass</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>

                       

        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                     
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Inicio</a></li>
                            <li class="active">Visualizar Funcionário</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                        <strong class="card-title">Funcionários</strong> 

                            </div>









                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>

                                            <th>Nome</th>
                                            <th>CPF</th>
                                            <th>Nascimento</th>
                                            <th>Expêriencia</th>
                                            <th>Setor</th>
                                            <th>Salário</th>
                                            <th>Senha<th>
                                            <th></th>
                                            <th></th>

                                            </tr>
                                           
                                            </thead>    

                                      <tbody>       
                                    <?php


                                    include 'backend/Model/bancodedados.php';

                                    $banco = new bancodedados();


                                    $select = "SELECT * FROM funcionario order by idFuncionario";

                                    $query = mysqli_query($banco->conectabanco(),$select);

                                    $dados = mysqli_fetch_assoc($query);

                                    $linhas = mysqli_num_rows($query);




                                     if ($linhas > 0){
                                       do{
                                       $id = $dados['idFuncionario'];
                                       $nome = $dados['nome'];
                                       $cpf  = $dados['cpf'];
                                       $nascimento = $dados['nascimento'];
                                       $experiencia = $dados['experiencias'];
                                       $setor= $dados ['setor'];
                                       $salario = $dados['salario'];
                                       $senha = $dados['senha'];




                                      echo "  

                                          

                                        <tr>
                                        
                                        <td id='nome$id'>$nome </td>
                                        <td id='cpf$id'>$cpf</td>
                                        <td id='nascimento$id'>$nascimento</td>
                                        <td id='experiencia$id'> $experiencia</td>
                                        <td id='setor$id'>$setor</td>
                                        <td id='salario$id'>$salario</td>
                                       <td> <input type='password' class='table table-striped table-bordered' 
                                       id='senha$id' value='$senha' size='10' disabled></td>    
                                       
                                     
         <td style='width:13%; '><button type='button' style='width: 100%' class='btn btn-outline-primary mb-1 editar' data-toggle='modal' data-target='#mediumModal' id='$id'>Editar</button>
  </td>




<td style='width:13%;'><button type='button' style='width: 100%'   data-toggle='modal' data-target='#smallmodal'    class='btn btn-outline-danger mb-1 excluir' id='$id'>   
                        Excluir</button> </td>

                        </tr>
                        ";
           
                                       }
        while ($dados = mysqli_fetch_assoc($query));
           }
        $banco->fechaBanco();

             ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


 




                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->
  

     <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mediumModalLabel">Edição de Dados</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">


                                <p>
                                 
                                 <form id="formularioEdicao" action="backend/Controller/reload.php" method="POST" >

                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nome</label></div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="nomeF" name="nome" placeholder="" class="form-control" value="">
                                            <small class="form-text text-muted"> Informe o nome do funcionário</small> </small></div>
                                    </div>

                                    <div class="row form-group">
                                            <div class="col col-md-3"><label for="date-input" class=" form-control-label">Nascimento</label></div>
                                            <div class="col-12 col-md-9">
                                                <input type="date" id="nascimentoF" name="nascimento" placeholder="00/00/0000" class="form-control" value="">
                                                <small class="form-text text-muted"> Informe a data de nascimento do funcionario</small> </small></div>
                                        </div>
                                     
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="email-input" class=" form-control-label">CPF</label></div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="cpfF" name="cpf" placeholder="" class="form-control" value="">
                                            <small class="form-text text-muted">Informe seu CPF</small></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="password-input" class=" form-control-label">Senha de Acesso </label></div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="passwordF" name="senha" placeholder="" class="form-control" value="">
                                            <small class="form-text text-muted">Senha para o funcionario acessar o sistema</small></div>
                                    </div>
                                    
                                    <div class="row form-group">
                                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Experiências</label></div>
                                            <div class="col-12 col-md-9">
                                                <textarea name="experiencia" id="experienciaF" rows="5" placeholder="" class="form-control" ></textarea>
                                            </div>
                                        </div>

                                         <div class="row form-group">
                                            <div class="col col-md-3"><label for="setor" class=" form-control-label">Setor</label></div>
                                            <div class="col-12 col-md-9">
                                                <select name="setor" id="selecaoSetor" class="form-control-sm form-control">
                                                    <option value="0" disabled selected>Escolha o Setor</option>
                                                    <option value="Lider de Área">Lider de Área</option>
                                                    <option value="Estoquista">Estoquista</option>
                                                    <option value="Vendedor">Vendedor</option>
                                                    

                                                </select>
                                                 
                                            
                                            </div>
                                              </div>



     <div class="row form-group">
    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Salário</label></div>
    <div class="col-12 col-md-9"><input type="text" id="salarioF" name="salario" placeholder="" class="form-control" value="">
        <small class="form-text text-muted"> Informe o salario do funcionario</small> </small>
  
                                             
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary cancelar" data-dismiss="modal">Cancelar</button>
                                <input type="submit" value="Editar" id='confirmarEdicao' name="confirmarFuncionario" class="btn btn-primary">
                                </div>
                                            </form>
                                            </div>
                                            </div>





                                
                            </div>
                            
                        </div>
                    </div>
                </div>





     
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>    
<script src="assets/js/valor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="assets/js/init-scripts/data-table/datatables-init.js"></script>
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
               <script src="vendors/jquery/dist/jquery.mask.js"></script>
                <script src="vendors/jquery/dist/jquery.mask.min.js"></script>
                <script src="vendors/jquery/src/jquery.mask.js"></script>


<!-- CONFIGURAÇÃO JQUERY DOS BOTÕES EDITAR EEXCLUIR-->
<script>
    
    
    
   $('.excluir').click(function(){
      id = this.id

       var confirma = confirm("Deseja excluir");  
       if (confirma == true){



      $.ajax({
        type: 'post',
        url: 'backend/Controller/excluirFuncionario.php',
        data: {'id': id},


        success: function(data) {
            
          location.reload();

             
        }

     
      });
   }
}); 





  $('.editar').click(function(){
      id = this.id;
      nome = $('#nome'+id).text();
      cpf = $('#cpf'+id).text();
      nascimento = $('#nascimento'+id).text();
      experiencia = $('#experiencia'+id).text();
      senha = $('#senha'+id).val();
      setor = $('#setor'+id).text();
      salario = $('#salario'+id).text();

     
    $('#formularioEdicao').find('input[id="nomeF"]').val(nome);
    $('#formularioEdicao').find('input[id="cpfF"]').val(cpf);
    $('#formularioEdicao').find('input[id="nascimentoF"]').val(nascimento);
    $('#formularioEdicao').find('input[id="passwordF"]').val(senha);
    $('#formularioEdicao').find('textarea[id="experienciaF"]').val(experiencia);
    $('#formularioEdicao').find('select[id="selecaoSetor"]').val(setor);
    $('#formularioEdicao').find('input[id="salarioF"]').val(salario);

$('#confirmarEdicao').click(function() {
    novoNome = $('#formularioEdicao').find('input[id="nomeF"]').val();
    novoCpf = $('#formularioEdicao').find('input[id="cpfF"]').val();
    novoNascimento = $('#formularioEdicao').find('input[id="nascimentoF"]').val(); 
    novaSenha = $('#formularioEdicao').find('input[id="passwordF"]').val();
    novaExperiencia =  $('#formularioEdicao').find('textarea[id="experienciaF"]').val();
    novoSetor = $('#formularioEdicao').find('select[id="selecaoSetor"]').val();
    novoSalario = $('#formularioEdicao').find('input[id="salarioF"]').val(); 
    
    $.ajax({
        type: 'post',
        url: 'backend/Controller/editarFuncionario.php',
        data: {
        'id': id, 
        'nome': novoNome, 
        'cpf':novoCpf, 
        'nascimento':novoNascimento, 
        'senha':novaSenha,
        'experiencia': novaExperiencia, 
        'setor':novoSetor, 
        'salario': novoSalario},
        success: function(data) {
           
        }

    });

})
  });


</script>


</body>


</html>
